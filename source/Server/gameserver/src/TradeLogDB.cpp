
#include "TradeLogDB.h"
#include "Config.h"

BOOL CTradeLogDB::Init() {
	T_B
		m_bInitOK = FALSE;

	if (g_Config.m_bTradeLogIsConfig) {
		_connect.enable_errinfo();

		printf("Connectting database [%s]......\n", g_Config.m_dsnTradeConnection.c_str());

		std::string err_info;
		bool r = _connect.connect(g_Config.m_dsnTradeConnection, err_info);
		if (!r) {
			LG("trade log db", "msgDatabase [%s] Connect Failed!, ERROR REPORT[%d]", g_Config.m_dsnTradeConnection.c_str(), err_info.c_str());
			return FALSE;
		}

		printf("Database Connected!\n");

		_tab_log = new CTradeTableLog(&_connect);

		if (!_tab_log)
			return FALSE;
	}

	m_bInitOK = TRUE;

	return TRUE;
	T_E
}

CTradeLogDB tradeLog_db;

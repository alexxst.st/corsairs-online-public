#define _HAS_STD_BYTE 0

#include "AccountServer2.h"

#include <stdlib.h>
#include <signal.h>
#include <CommCtrl.h>
#include "GlobalVariable.h"
#include "inifile.h"
#include "ResourceBundleManage.h"
#include "i18n.h"

#define AUTHUPDATE_TIMER 1
#define GROUPUPDATE_TIMER 2

ThreadPool* comm = NULL;
ThreadPool* proc = NULL;
AuthThreadPool atp;
bool bExit = false;

//#pragma init_seg( lib )
CResourceBundleManage g_ResourceBundleManage("AccountServer.loc"); //Add by lark.li 20080330

//BillThread bt;

//std::string g_BTSAddr[2] = {"61.152.115.79:7243", "61.152.115.79:7243"}; 旧的BTS地址
//std::string g_BTSAddr[2] = {"61.152.115.172:7243", "61.152.115.173:7243"};

void __cdecl Ctrlc_Dispatch(int sig) {
	if (!bExit) {
		C_PRINT(RES_STRING(AS_MAIN_CPP_00007));
		PostMessage(g_hMainWnd, WM_CLOSE, 0, 0);
		bExit = TRUE;
	}
}

//#include <string.h>

HANDLE hConsole = NULL;

int main(int argc, char* argv[]) {
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	C_PRINT("AccountServer 1.38 (by: Sheep Squad)\n");
	C_TITLE("AccountServer.exe")

	SEHTranslator translator;

	T_B

		// 创建主窗口
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); //Add by Arcol (2005-12-2)

	C_TITLE("AccountServer.exe")

	signal(SIGINT, Ctrlc_Dispatch);
	g_MainThreadID = GetCurrentThreadId();

	if (!g_MainDBHandle.CreateObject()) {
		C_PRINT("failed\n");
		//printf("Main database handler create failed, AccountServer hang!\n");
		printf(RES_STRING(AS_MAIN_CPP_00003));
		system("pause");
		return -1;
	}

	// 初始化BTI
	//if (!g_BillThread.CreateBillingSystem(g_hMainWnd))
	//if (!g_BillService.CreateBillingSystem(g_hMainWnd)) {
	//	//printf("Cannot Create Billing System, AccountServer hang!\n");
	//	printf(RES_STRING(AS_MAIN_CPP_00004));
	//	system("pause");
	//	return -1;
	//}

	//if (!g_TomService.InitService()) {
	//	//printf("Cannot initialize Tom Service System, AccountServer hang!\n");
	//	printf(RES_STRING(AS_MAIN_CPP_00005));
	//	system("pause");
	//	return -1;
	//}

	// 启动认证计费线程
	//bt.Launch();
	//g_BillThread.Launch();
	atp.Launch();

	// 初始化网络
	TcpCommApp::WSAStartup();
	comm = ThreadPool::CreatePool(10, 10, 256);

	int nProcCnt = 2 * AuthThreadPool::AT_MAXNUM;
	proc = ThreadPool::CreatePool(nProcCnt, nProcCnt, 2048);
	try {
		g_As2 = new AccountServer2(proc, comm);
	} catch (excp& e) {
		printf("%s\n", e.what());
		comm->DestroyPool();
		TcpCommApp::WSACleanup();
		Sleep(10 * 1000);
		return -1;
	} catch (...) {
		//printf("AccountServer初始化期间发生未知错误，请通知开发者\n");
		printf(RES_STRING(AS_MAIN_CPP_00006));
		comm->DestroyPool();
		TcpCommApp::WSACleanup();
		Sleep(10 * 1000);
		return -2;
	}

	// 消息循环
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// 卸载网络
	delete g_As2;
	if (comm != NULL)
		comm->DestroyPool();
	if (proc != NULL)
		proc->DestroyPool();
	TcpCommApp::WSACleanup();

	// 认证、计费线程退出
	//bt.NotifyToExit();
	//g_BillThread.NotifyToExit();
	atp.NotifyToExit();

	atp.WaitForExit();
	//g_BillThread.WaitForExit(-1);

	// 记录最后状态
	LG("RunLabel", "\n");
	for (char i = 0; i < AuthThreadPool::AT_MAXNUM; ++i) {
		LG("RunLabel", "%02d %04d\n", AuthThreadPool::RunLabel[i],
		   AuthThreadPool::RunConsume[i]);
	}
	LG("RunLabel", "\n");

	g_MainDBHandle.ReleaseObject();

	T_FINAL

	return 0;
}

#include "StdAfx.h"
#include "Tools.h"
#include <strstream>

string ConvertNumToChinese(int num) {
	if (num == 0)
		return CLanguageRecordInstance().GetString(421);

	char szNum[255] = {0};
	itoa(num, szNum, 10);

	char szBuf[255] = {0};
	std::ostrstream str(szBuf, sizeof(szBuf));

	const char* pszPos = szNum;
	if (*pszPos == '-') {
		str << CLanguageRecordInstance().GetString(422);
		pszPos++;
	}

	static char szChinese[10][16]; // = { CLanguageRecordInstance().GetString(421), CLanguageRecordInstance().GetString(423), CLanguageRecordInstance().GetString(424), CLanguageRecordInstance().GetString(425), CLanguageRecordInstance().GetString(426), CLanguageRecordInstance().GetString(427), CLanguageRecordInstance().GetString(428), CLanguageRecordInstance().GetString(429), CLanguageRecordInstance().GetString(430), CLanguageRecordInstance().GetString(431) };
	strcpy(szChinese[0], CLanguageRecordInstance().GetString(421));
	strcpy(szChinese[1], CLanguageRecordInstance().GetString(423));
	strcpy(szChinese[2], CLanguageRecordInstance().GetString(424));
	strcpy(szChinese[3], CLanguageRecordInstance().GetString(425));
	strcpy(szChinese[4], CLanguageRecordInstance().GetString(426));
	strcpy(szChinese[5], CLanguageRecordInstance().GetString(427));
	strcpy(szChinese[6], CLanguageRecordInstance().GetString(428));
	strcpy(szChinese[7], CLanguageRecordInstance().GetString(429));
	strcpy(szChinese[8], CLanguageRecordInstance().GetString(430));
	strcpy(szChinese[9], CLanguageRecordInstance().GetString(431));

	static char szHigh[8][16]; // = { CLanguageRecordInstance().GetString(432), CLanguageRecordInstance().GetString(433), CLanguageRecordInstance().GetString(434), CLanguageRecordInstance().GetString(435), CLanguageRecordInstance().GetString(432), CLanguageRecordInstance().GetString(433), CLanguageRecordInstance().GetString(434), CLanguageRecordInstance().GetString(436) };
	strcpy(szHigh[0], CLanguageRecordInstance().GetString(432));
	strcpy(szHigh[1], CLanguageRecordInstance().GetString(433));
	strcpy(szHigh[2], CLanguageRecordInstance().GetString(434));
	strcpy(szHigh[3], CLanguageRecordInstance().GetString(435));
	strcpy(szHigh[4], CLanguageRecordInstance().GetString(432));
	strcpy(szHigh[5], CLanguageRecordInstance().GetString(433));
	strcpy(szHigh[6], CLanguageRecordInstance().GetString(434));
	strcpy(szHigh[7], CLanguageRecordInstance().GetString(436));

	char nChar = 0;
	int nZeroNum = 0;
	bool IsBigMark = false; // 用于万，亿的特殊处理，在全是零时
	int nHigh = 0;
	int nLen = 0;
	while (*pszPos) {
		nChar = *pszPos++;
		nLen = (int)strlen(pszPos);

		// 中部的多个零合并为一个零,且尾部的零也不显示
		if (nChar == '0') {
			nZeroNum++;
			if (IsBigMark && (nLen == 4 || nLen == 8)) {
				str << szHigh[nLen - 1];
				IsBigMark = false;
			}
			continue;
		} else {
			IsBigMark = true;
			if (nZeroNum > 0) // 从零开始后,碰到的第一个非零
			{
				nZeroNum = 0;
				str << CLanguageRecordInstance().GetString(421);
			}
			str << szChinese[nChar - '0'];
			if (nLen > 0) {
				if (nLen < 9) {
					nHigh = nLen - 1;
				} else {
					nLen = nLen % 9;
					nHigh = nLen;
				}
				str << szHigh[nHigh];

				// 万，亿的特殊处理
				if (nHigh == 3 || nHigh == 7) {
					IsBigMark = false;
				}
			}
		}
	}
	str << std::ends;

	// 头为一十,省略一
	string rv = str.str();
	if (rv.length() >= 4 && rv.substr(0, 4) == CLanguageRecordInstance().GetString(437))
		return rv.substr(2, rv.length());

	return rv;
}

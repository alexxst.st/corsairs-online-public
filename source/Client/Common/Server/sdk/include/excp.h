#ifndef DBCEXCP_H
#define DBCEXCP_H

#include <exception>
#include <string.h>
#include "DBCCommon.h"
#include <sstream>
#include <string_view>

_DBC_BEGIN
#pragma pack(push)
#pragma pack(4)

//------------------------------------------------------------------------------------------------------------------
//common exception define
class excp : public std::exception // 基异常类
{
public:
	excp(const std::stringstream& desc) : m_Buffer{desc.str()} {}
	excp(const char* desc) : m_Buffer{desc} {}
	virtual ~excp() = default;
	virtual const char* what() const { return m_Buffer.c_str(); }

private:
	void setZero() {
		std::exception::operator=(std::exception());
		m_Buffer.clear();
	}

	std::string m_Buffer;
};

//------------------------------------------------------------------------------------------------------------------
class excpMem : public excp //内存分配或释放异常
{
public:
	excpMem(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpArr : public excp //数组越界或其他数组相关错误异常
{
public:
	excpArr(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpSync : public excp //操作系统同步对象操作异常
{
public:
	excpSync(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpThrd : public excp //操作系统线程操作异常
{
public:
	excpThrd(const std::stringstream& desc) : excp(desc){};
};
class excpSock : public excp //操作系统线程操作异常
{
public:
	excpSock(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpCOM : public excp //COM操作异常
{
public:
	excpCOM(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpDB : public excp //数据库操作异常
{
public:
	excpDB(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpIniF : public excp //文件操作异常
{
public:
	excpIniF(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpFile : public excp //文件操作异常
{
public:
	excpFile(const std::stringstream& desc) : excp(desc){};
};
//------------------------------------------------------------------------------------------------------------------
class excpXML : public excp //文件操作异常
{
public:
	excpXML(const std::stringstream& desc) : excp(desc){};
};

#pragma pack(pop)
_DBC_END

//===================================================================================================================================

#define THROW_EXCP(EXCP, DESC) throw EXCP(std::stringstream("File:") << __FILE__ << " Line:" << __LINE__ << " desc:" << DESC); // 程序中抛异常所用的宏定义

#endif

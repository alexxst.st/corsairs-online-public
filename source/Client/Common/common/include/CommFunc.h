//=============================================================================
// FileName: CommFunc.h
// Creater: ZhangXuedong
// Date: 2005.01.06
// Comment:
//	2005.4.28	Arcol	add the text filter manager class: CTextFilter
//=============================================================================

#ifndef COMMFUNC_H
#define COMMFUNC_H

#include "CompCommand.h"
#include "SkillRecord.h"
#include "CharacterRecord.h"
#include "ItemRecord.h"
#include "ItemAttrType.h"
#include "JobType.h"
#include "NetRetCode.h"

extern bool KitbagStringConv(short sKbCapacity, std::string& strData);

//=============================================================================
/*---------------------------------------------------------------
 * УГНѕ:УГУЪјмІвґґЅЁµДЅЗЙ«Нв№ЫКэѕЭКЗ·сєП·Ё
 * nPart - ¶ФУ¦Нв№ЫID,nValue - Нв№ЫµДЦµ
 * ·µ»ШЦµЈєНв№ЫКэѕЭКЗ·сєП·ЁЎЈ
 */
extern bool g_IsValidLook(int nType, int nPart, long nValue);

/*---------------------------------------------------------------
 * ulAreaMask ЗшУтАаРН
 * ·µ»ШЦµЈєtrue єЈСуЎЈfalse ВЅµШ
 */
inline bool g_IsSea(unsigned short usAreaMask) {
	return !(usAreaMask & enumAREA_TYPE_LAND);
}

inline bool g_IsLand(unsigned short usAreaMask) {
	return (usAreaMask & enumAREA_TYPE_LAND) || (usAreaMask & enumAREA_TYPE_BRIDGE);
}

// ёщѕЭґ«ИлµДЧуУТКЦµАѕЯID
// ·µ»ШїЙТФК№УГµДД¬ИПјјДЬ,·µ»Ш-1,Г»УРјјДЬ
extern int g_GetItemSkill(int nLeftItemID, int nRightItemID);

extern BOOL IsDist(int x1, int y1, int x2, int y2, DWORD dwDist);

// КЗ·сХэИ·µДјјДЬДї±к
extern int g_IsRightSkillTar(int nTChaCtrlType, bool bTIsDie, bool bTChaBeSkilled, int nTChaArea,
							 int nSChaCtrlType, int nSSkillObjType, int nSSkillObjHabitat, int nSSkillEffType,
							 bool bIsTeammate, bool bIsFriend, bool bIsSelf);

/*---------------------------------------------------------------
 * ІОКэ:ЧуКЦЈ¬УТКЦЈ¬ЙнМеµДµАѕЯIDЎЈјјДЬ±аєЕЎЈ
 * ·µ»ШЦµ:1-їЙК№УГ,0-І»їЙК№УГ,-1-јјДЬІ»ґжФЪ
 */
extern int g_IsUseSkill(stNetChangeChaPart* pSEquip, int nSkillID);
extern bool g_IsRealItemID(int nItemID);

inline int g_IsUseSkill(stNetChangeChaPart* pSEquip, CSkillRecord* p) {
	if (!p)
		return -1;

	return g_IsUseSkill(pSEquip, p->nID);
}

inline int g_IsUseSeaLiveSkill(long lFitNo, CSkillRecord* p) {
	if (!p)
		return -1;

	for (int i = 0; i < defSKILL_ITEM_NEED_NUM; i++) {
		if (p->sItemNeed[0][i][0] == cchSkillRecordKeyValue)
			break;

		if (p->sItemNeed[0][i][0] == enumSKILL_ITEM_NEED_ID) {
			if (p->sItemNeed[0][i][1] == lFitNo)
				return 1;
		}
	}

	return 0;
}

inline bool g_IsPlyCtrlCha(int nChaCtrlType) {
	if (nChaCtrlType == enumCHACTRL_PLAYER || nChaCtrlType == enumCHACTRL_PLAYER_PET)
		return true;
	return false;
}

inline bool g_IsMonsCtrlCha(int nChaCtrlType) {
	if (nChaCtrlType == enumCHACTRL_MONS || nChaCtrlType == enumCHACTRL_MONS_TREE || nChaCtrlType == enumCHACTRL_MONS_MINE || nChaCtrlType == enumCHACTRL_MONS_FISH || nChaCtrlType == enumCHACTRL_MONS_DBOAT || nChaCtrlType == enumCHACTRL_MONS_REPAIRABLE)
		return true;
	return false;
}

inline bool g_IsNPCCtrlCha(int nChaCtrlType) {
	if (nChaCtrlType == enumCHACTRL_NPC || nChaCtrlType == enumCHACTRL_NPC_EVENT)
		return true;
	return false;
}

inline bool g_IsChaEnemyCtrlSide(int nSCtrlType, int nTCtrlType) {
	if (g_IsPlyCtrlCha(nSCtrlType) & g_IsPlyCtrlCha(nTCtrlType))
		return false;
	if (g_IsMonsCtrlCha(nSCtrlType) & g_IsMonsCtrlCha(nTCtrlType))
		return false;
	return true;
}

inline bool g_IsFileExist(const char* szFileName) {
	FILE* fp = NULL;
	if (NULL == (fp = fopen(szFileName, "rb")))
		return false;
	if (fp)
		fclose(fp);
	return true;
}

extern char* LookData2String(const stNetChangeChaPart* pLook, char* szLookBuf, int nLen, bool bNewLook = true);
extern bool Strin2LookData(stNetChangeChaPart* pLook, std::string& strData);
extern char* ShortcutData2String(const stNetShortCut* pShortcut, char* szShortcutBuf, int nLen);
extern bool String2ShortcutData(stNetShortCut* pShortcut, std::string& strData);

inline long g_ConvItemAttrTypeToCha(long lItemAttrType) {
	if (lItemAttrType >= ITEMATTR_COE_STR && lItemAttrType <= ITEMATTR_COE_PDEF)
		return lItemAttrType + (ATTR_ITEMC_STR - ITEMATTR_COE_STR);
	else if (lItemAttrType >= ITEMATTR_VAL_STR && lItemAttrType <= ITEMATTR_VAL_PDEF)
		return lItemAttrType + (ATTR_ITEMV_STR - ITEMATTR_VAL_STR);
	else
		return 0;
}

// ¶ФУ¦ЗшУтАаРНµДІОКэёцКэ
inline short g_GetRangeParamNum(char RangeType) {
	short sParamNum = 0;
	switch (RangeType) {
	case enumRANGE_TYPE_STICK:
		sParamNum = 2;
		break;
	case enumRANGE_TYPE_FAN:
		sParamNum = 2;
		break;
	case enumRANGE_TYPE_SQUARE:
		sParamNum = 1;
		break;
	case enumRANGE_TYPE_CIRCLE:
		sParamNum = 1;
		break;
	}

	return sParamNum + 1;
}

//=============================================================================
// chChaType ЅЗЙ«АаРН
// chChaTerrType ЅЗЙ«»о¶ЇїХјдµДАаРН
// bIsBlock КЗ·сХП°­
// ulAreaMask ЗшУтАаРН
// ·µ»ШЦµЈєtrue їЙФЪёГµҐФЄЙПТЖ¶ЇЎЈfalse І»їЙТЖ¶Ї
//=============================================================================
inline bool g_IsMoveAble(char chChaCtrlType, char chChaTerrType, unsigned short usAreaMask) {
	bool bRet1 = false;
	if (chChaTerrType == defCHA_TERRITORY_DISCRETIONAL)
		bRet1 = true;
	else if (chChaTerrType == defCHA_TERRITORY_LAND) {
		if (usAreaMask & enumAREA_TYPE_LAND || usAreaMask & enumAREA_TYPE_BRIDGE)
			bRet1 = true;
	} else if (chChaTerrType == defCHA_TERRITORY_SEA) {
		if (!(usAreaMask & enumAREA_TYPE_LAND))
			bRet1 = true;
	}

	bool bRet2 = true;
	if (usAreaMask & enumAREA_TYPE_NOT_FIGHT) // ·ЗХЅ¶·ЗшУт
	{
		if (g_IsMonsCtrlCha(chChaCtrlType))
			bRet2 = false;
	}

	return bRet1 && bRet2;
}

inline const char* g_GetJobName(short sJobID) {
	if (sJobID < 0 || sJobID >= MAX_JOB_TYPE)
		return g_szJobName[0];

	return g_szJobName[sJobID];
}

inline short g_GetJobID(const char* szJobName) {
	for (short i = 0; i < MAX_JOB_TYPE; i++) {
		if (!strcmp(g_szJobName[i], szJobName))
			return i;
	}

	return 0;
}

inline const char* g_GetCityName(short sCityID) {
	if (sCityID < 0 || sCityID >= defMAX_CITY_NUM)
		return "";

	return g_szCityName[sCityID];
}

inline short g_GetCityID(const char* szCityName) {
	for (short i = 0; i < defMAX_CITY_NUM; i++) {
		if (!strcmp(g_szCityName[i], szCityName))
			return i;
	}

	return -1;
}

inline bool g_IsSeatPose(int pose) {
	return 16 == pose;
}

// Тэ·ў±нПЦ·µ»ШХж
inline bool g_IsValidFightState(int nState) {
	return nState < enumFSTATE_TARGET_NO;
}

inline bool g_ExistStateIsDie(char chState) {
	if (chState >= enumEXISTS_WITHERING)
		return true;

	return false;
}

inline const char* g_GetItemAttrExplain(int v) {
	switch (v) {
	case ITEMATTR_COE_STR:
		return "Strength Bonus"; // "Б¦БїјУіЙ";
	case ITEMATTR_COE_AGI:
		return "Agility Bonus"; // "ГфЅЭјУіЙ";
	case ITEMATTR_COE_DEX:
		return "Accuracy Bonus"; // "ЧЁЧўјУіЙ";
	case ITEMATTR_COE_CON:
		return "Constitution Bonus"; // "МеЦКјУіЙ";
	case ITEMATTR_COE_STA:
		return "Spirit Bonus"; // "ѕ«ЙсјУіЙ";
	case ITEMATTR_COE_LUK:
		return "Luck Bonus"; // "РТФЛјУіЙ";
	case ITEMATTR_COE_ASPD:
		return "Attack Rate Bonus"; // "№Ґ»чЖµВКјУіЙ";
	case ITEMATTR_COE_ADIS:
		return "Attack range Bonus"; // "№Ґ»чѕаАлјУіЙ";
	case ITEMATTR_COE_MNATK:
		return "Minimum attack Bonus"; // "ЧоРЎ№Ґ»чБ¦јУіЙ";
	case ITEMATTR_COE_MXATK:
		return "Maximum attack Bonus"; // "Чоґу№Ґ»чБ¦јУіЙ";
	case ITEMATTR_COE_DEF:
		return "Defense Bonus"; // "·АУщјУіЙ";
	case ITEMATTR_COE_MXHP:
		return "Maximum HP Bonus"; // "ЧоґуHPјУіЙ";
	case ITEMATTR_COE_MXSP:
		return "Maximum SP Bonus"; // "ЧоґуSPјУіЙ";
	case ITEMATTR_COE_FLEE:
		return "Dodge rate Bonus"; // "ЙБ±ЬВКјУіЙ";
	case ITEMATTR_COE_HIT:
		return "Hit rate Bonus"; // "ГьЦРВКјУіЙ";
	case ITEMATTR_COE_CRT:
		return "Critical hitrate Bonus"; // "±¬»чВКјУіЙ";
	case ITEMATTR_COE_MF:
		return "Drop rate Bonus"; // "С°±¦ВКјУіЙ";
	case ITEMATTR_COE_HREC:
		return "HP recovery speed Bonus"; // "HP»ЦёґЛЩ¶ИјУіЙ";
	case ITEMATTR_COE_SREC:
		return "SP Recovery rate Bonus"; // "SP»ЦёґЛЩ¶ИјУіЙ";
	case ITEMATTR_COE_MSPD:
		return "Movement speed Bonus"; // "ТЖ¶ЇЛЩ¶ИјУіЙ";
	case ITEMATTR_COE_COL:
		return "Material Mining Speed Bonus"; // "ЧКФґІЙјЇЛЩ¶ИјУіЙ";

	case ITEMATTR_VAL_STR:
		return "Strength Bonus"; // "Б¦БїјУіЙ";
	case ITEMATTR_VAL_AGI:
		return "Agility Bonus"; // "ГфЅЭјУіЙ";
	case ITEMATTR_VAL_DEX:
		return "Accuracy Bonus"; // "ЧЁЧўјУіЙ";
	case ITEMATTR_VAL_CON:
		return "Constitution Bonus"; // "МеЦКјУіЙ";
	case ITEMATTR_VAL_STA:
		return "Spirit Bonus"; // "ѕ«ЙсјУіЙ";
	case ITEMATTR_VAL_LUK:
		return "Luck Bonus"; // "РТФЛјУіЙ";
	case ITEMATTR_VAL_ASPD:
		return "Attack Rate Bonus"; // "№Ґ»чЖµВКјУіЙ";
	case ITEMATTR_VAL_ADIS:
		return "Attack range Bonus"; // "№Ґ»чѕаАлјУіЙ";
	case ITEMATTR_VAL_MNATK:
		return "Minimum attack Bonus"; // "ЧоРЎ№Ґ»чБ¦јУіЙ";
	case ITEMATTR_VAL_MXATK:
		return "Maximum attack Bonus"; // "Чоґу№Ґ»чБ¦јУіЙ";
	case ITEMATTR_VAL_DEF:
		return "Defense Bonus"; // "·АУщјУіЙ";
	case ITEMATTR_VAL_MXHP:
		return "Maximum HP Bonus"; // "ЧоґуHPјУіЙ";
	case ITEMATTR_VAL_MXSP:
		return "Maximum SP Bonus"; // "ЧоґуSPјУіЙ";
	case ITEMATTR_VAL_FLEE:
		return "Dodge rate Bonus"; // "ЙБ±ЬВКјУіЙ";
	case ITEMATTR_VAL_HIT:
		return "Hit rate Bonus"; // "ГьЦРВКјУіЙ";
	case ITEMATTR_VAL_CRT:
		return "Critical hitrate Bonus"; // "±¬»чВКјУіЙ";
	case ITEMATTR_VAL_MF:
		return "Drop rate Bonus"; // "С°±¦ВКјУіЙ";
	case ITEMATTR_VAL_HREC:
		return "HP recovery speed Bonus"; // "HP»ЦёґЛЩ¶ИјУіЙ";
	case ITEMATTR_VAL_SREC:
		return "SP Recovery rate Bonus"; // "SP»ЦёґЛЩ¶ИјУіЙ";
	case ITEMATTR_VAL_MSPD:
		return "Movement speed Bonus"; // "ТЖ¶ЇЛЩ¶ИјУіЙ";
	case ITEMATTR_VAL_COL:
		return "Material Mining Speed Bonus"; // "ЧКФґІЙјЇЛЩ¶ИјУіЙ";

	case ITEMATTR_VAL_PDEF:
		return "Physical resist Bonus"; // "ОпАнµЦї№јУіЙ";
	case ITEMATTR_MAXURE:
		return "Max durability"; // "ЧоґуДНѕГ¶И";
	case ITEMATTR_MAXENERGY:
		return "Max energy"; // "ЧоґуДЬБї";
	default:
		return "Unknown tools characteristics"; // "ОґЦЄµАѕЯКфРФ";
	}
}

inline const char* g_GetServerError(int error_code) // ЅвОцґнОуВл
{
	switch (error_code) {
	case ERR_AP_INVALIDUSER:
		return "Invalid Account"; // "ОЮР§ХЛєЕ";
	case ERR_AP_INVALIDPWD:
		return "Password incorrect"; // "ГЬВлґнОу";
	case ERR_AP_ACTIVEUSER:
		return "Account activation failed"; // "ј¤»оУГ»§К§°Ь";
	case ERR_AP_DISABLELOGIN:
		return "Your cha is currently in logout save mode, please try logging in again later."; // "ДїЗ°ДъµДЅЗЙ«Хэґ¦УЪПВПЯґжЕМ№эіМЦРЈ¬ЗлЙФєуФЩіўКФµЗВјЎЈ";
	case ERR_AP_LOGGED:
		return "This account is already online"; // "ґЛХКєЕТСґ¦УЪµЗВјЧґМ¬";
	case ERR_AP_BANUSER:
		return "Account has been banned"; // "ХКєЕТС·вНЈ";
	case ERR_AP_GPSLOGGED:
		return "This GroupServer has login"; // "ґЛGroupServerТСµЗВј";
	case ERR_AP_GPSAUTHFAIL:
		return "This GroupServer Verification failed"; // "ґЛGroupServerИПЦ¤К§°Ь";
	case ERR_AP_SAVING:
		return "Saving your character, please try again in 15 seconds..."; // "ХэФЪ±ЈґжДгµДЅЗЙ«Ј¬Зл15ГлєуЦШКФ...";
	case ERR_AP_LOGINTWICE:
		return "Your account is logged on far away"; // "ДгµДХЛєЕФЪФ¶ґ¦ФЩґОµЗВј";
	case ERR_AP_ONLINE:
		return "Your account is already online"; // "ДгµДХЛєЕТСФЪПЯ";
	case ERR_AP_DISCONN:
		return "GroupServer disconnected"; // "GroupServerТС¶ПїЄ";
	case ERR_AP_UNKNOWNCMD:
		return "unknown agreement, don\'t deal with"; // "ОґЦЄР­ТйЈ¬І»ґ¦Ан";
	case ERR_AP_TLSWRONG:
		return "local saving error"; // "±ѕµШґжґўґнОу";
	case ERR_AP_NOBILL:
		return "This account has expired, please topup!"; // "ґЛХЛєЕТС№эЖЪЈ¬ЗлідЦµЈЎ";

	case ERR_PT_LOGFAIL:
		return "GateServer to GroupServer login failed"; // "GateServerПтGroupServerµДµЗВјК§°Ь";
	case ERR_PT_SAMEGATENAME:
		return "GateServer and login GateServer have similar name"; // "GateServerУлТСµЗВјGateServerЦШГы";

	case ERR_PT_INVALIDDAT:
		return "Ineffective data model"; // "ОЮР§µДКэѕЭёсКЅ";
	case ERR_PT_INERR:
		return "server link operation integrality error "; // "·юОсЖчЦ®јдµДІЩЧчНкХыРФґнОу";
	case ERR_PT_NETEXCP:
		return "Account server has encountered a malfunction"; // "ХКєЕ·юОсЖч№КХП";	// GroupServer·ўПЦµДµЅAccuntServerµДНшВз№КХП
	case ERR_PT_DBEXCP:
		return "database server malfunction"; // "КэѕЭїв·юОсЖч№КХП";	// GroupServer·ўПЦµДµЅDatabaseµД№КХП
	case ERR_PT_INVALIDCHA:
		return "Current account does not have a request (Select/Delete) to character"; // "µ±З°ХКєЕІ»УµУРЗлЗу(СЎФс/Йѕіэ)µДЅЗЙ«";
	case ERR_PT_TOMAXCHA:
		return "reached the maximum number of characters you can create"; // "ТСѕ­ґпµЅЧо¶аДЬґґЅЁµДЅЗЙ«КэБЛ";
	case ERR_PT_SAMECHANAME:
		return "Character name already exist"; // "ЦШёґµДЅЗЙ«Гы";
	case ERR_PT_INVALIDBIRTH:
		return "illegal birth place"; // "·З·ЁµДіцЙъµШ";
	case ERR_PT_TOOBIGCHANM:
		return "Character name is too long"; // "ЅЗЙ«ГыМ«і¤";
	case ERR_PT_ISGLDLEADER:
		return "Guild must have a leader, please disband your guild first then delete your character"; // "№«»бІ»їЙТ»ИХОЮі¤Ј¬ЗлПИЅвЙў№«»бФЩЙѕіэЅЗЙ«";
	case ERR_PT_ERRCHANAME:
		return "Illegal character name"; // "·З·ЁµДЅЗЙ«ГыіЖ";
	case ERR_PT_SERVERBUSY:
		return "System is busy, please try again later"; // "ПµНіГ¦Ј¬ЗлЙФєуФЩКФ";
	case ERR_PT_TOOBIGPW2:
		return "second code length illegal"; // "¶юґОГЬВлі¤¶И·З·Ё";
	case ERR_PT_INVALID_PW2:
		return "Cha second password not created"; // "ОґґґЅЁЅЗЙ«±Ј»¤¶юґОГЬВл";
	case ERR_PT_BADBOY:
		return "My child, you are very bold. You have been reported to the authority. Please do not commit the offense again!"; // "єўЧУЈ¬ДгєЬBTЈ¬ТСѕ­ЧФ¶Ї¶ФДгЧчіцБЛНЁ±ЁЕъЖАЈ¬ТЄТэТФОЄЅдЈ¬І»їЙФЩ·ёЈЎ";

	case ERR_MC_NETEXCP:
		return "Discovered exceptional line error on GateServer"; // "GateServer·ўПЦµДНшВзТміЈ";
	case ERR_MC_NOTSELCHA:
		return "current not yet handled character state"; // "µ±З°Оґґ¦УЪСЎФсЅЗЙ«ЧґМ¬";
	case ERR_MC_NOTPLAY:
		return "Currently not in gameplay, unable to send ENDPLAY command"; // "µ±З°І»ґ¦УЪНжУОП·ЧґМ¬Ј¬І»ДЬ·ўЛНENDPLAYГьБо";
	case ERR_MC_NOTARRIVE:
		return "target map cannot be reached"; // "Дї±кµШНјІ»їЙµЅґп";
	case ERR_MC_TOOMANYPLY:
		return "This server is currently full, please select another server!"; // "±ѕ·юОсЖчЧйИЛКэТСВъ, ЗлСЎФсЖдЛьЧйЅшРРУОП·!";
	case ERR_MC_NOTLOGIN:
		return "Youa re not login"; // "ДгЙРОґµЗВЅ";
	case ERR_MC_VER_ERROR:
		return "Client version error, server refused connection!"; // "їН»§¶ЛµД°ж±ѕєЕґнОу,·юОсЖчѕЬѕшµЗВјЈЎ";
	case ERR_MC_ENTER_ERROR:
		return "failed to enter map!"; // "ЅшИлµШНјК§°ЬЈЎ";
	case ERR_MC_ENTER_POS:
		return "Map position illegal, you\'ll be sent back to your birth city, please relog!"; // "µШНјО»ЦГ·З·ЁЈ¬ДъЅ«±»ЛН»ШіцЙъіЗКРЈ¬ЗлЦШРВµЗВЅЈЎ";

	case ERR_TM_OVERNAME:
		return "GameServer name repeated"; // "GameServerГыЦШёґ";
	case ERR_TM_OVERMAP:
		return "GameServerMapNameRepeated"; // "GameServerЙПµШНјГыЦШёґ";
	case ERR_TM_MAPERR:
		return "GameServer map assign language error"; // "GameServerµШНјЕдЦГУп·ЁґнОу";

	case ERR_SUCCESS:
		return "Jack is too BT, correct also will ask me if anything is wrong!"; // "JackМ«BTБЛЈ¬ХэИ·ТІАґОКОТКІГґґнОуЈЎ";
	default: {
		int l_error_code = error_code;
		l_error_code /= 500;
		l_error_code *= 500;
		static char l_buffer[500];
		char l_convt[20];
		switch (l_error_code) {
		case ERR_MC_BASE:
			return strcat(strcpy(l_buffer, itoa(error_code, l_convt, 10)), "(GameServer/GateServer->Client return error code space 1-500)"); //"(GameServer/GateServer->Client·µ»ШµДґнОуВлїХјд1Ј­500)");
		case ERR_PT_BASE:
			return strcat(strcpy(l_buffer, itoa(error_code, l_convt, 10)), "(GroupServer->GateServer return error code range 501-1000)"); //"(GroupServer->GateServer·µ»ШµДґнОуВлїХјд501Ј­1000)");
		case ERR_AP_BASE:
			return strcat(strcpy(l_buffer, itoa(error_code, l_convt, 10)), "(AccountServer->GroupServe return error code from 1001-1500)"); //"(AccountServer->GroupServer·µ»ШµДґнОуВлїХјд1001Ј­1500)");
		case ERR_MT_BASE:
			return strcat(strcpy(l_buffer, itoa(error_code, l_convt, 10)), "(GameServer->GateServer return error code range 1501-2000)"); //"(GameServer->GateServer·µ»ШµДґнОуВлїХјд1501Ј­2000)");
		default:
			return strcat(strcpy(l_buffer, itoa(error_code, l_convt, 10)), "(Jack is too insane, he made a mistake that I don\'t even know.)"); //"(JackМ«BTБЛЈ¬ЕЄёцґнОуБ¬ОТ¶јІ»ИПК¶ЎЈ)");
		}
	}
	}
}

inline bool isNumeric(const char* name, unsigned short len) {
	const unsigned char* l_name = reinterpret_cast<const unsigned char*>(name);
	if (len == 0) {
		return false;
	}
	for (unsigned short i = 0; i < len; i++) {
		if (!l_name[i] || !isdigit(l_name[i])) {
			return false;
		}
	}
	return true;
}

inline bool isAlphaNumeric(const char* name, unsigned short len) {
	const unsigned char* l_name = reinterpret_cast<const unsigned char*>(name);
	if (len == 0) {
		return false;
	}
	for (unsigned short i = 0; i < len; i++) {
		if (!l_name[i] || !isalnum(l_name[i])) {
			return false;
		}
	}
	return true;
}

// ±ѕєЇКэ№¦ДЬ°ьАЁјмІйЧЦ·ыґ®ЦРGBKЛ«ЧЦЅЪєєЧЦЧЦ·ыµДНкХыРФЎўНшВз°ьЦРЧЦ·ыґ®µДНкХыРФµИЎЈ
// nameОЄЦ»ФКРнУРґуРЎРґЧЦДёКэЧЦєНєєЧЦЈЁИҐіэИ«ЅЗїХёсЈ©ІЕ·µ»Шtrue;
// lenІОКэОЄЧЦ·ыґ®nameµДі¤¶И=strlen(name),І»°ьАЁЅбОІNULLЧЦ·ыЎЈ
inline bool IsValidName(const char* name, unsigned short len) {
	const unsigned char* l_name = reinterpret_cast<const unsigned char*>(name);
	bool l_ishan = false;
	// if (len == 0)
	//	return 0;
	for (unsigned short i = 0; i < len; i++) {
		if (!l_name[i]) {
			return false;
		} else if (l_ishan) {
			if (l_name[i - 1] == 0xA1 && l_name[i] == 0xA1) // №эВЛИ«ЅЗїХёс
			{
				return false;
			}
			if (l_name[i] > 0x3F && l_name[i] < 0xFF && l_name[i] != 0x7F) {
				l_ishan = false;
			} else {
				return false;
			}
		} else if (l_name[i] > 0x80 && l_name[i] < 0xFF) {
			l_ishan = true;
		} else if ((l_name[i] >= 'A' && l_name[i] <= 'Z') || (l_name[i] >= 'a' && l_name[i] <= 'z') || (l_name[i] >= '0' && l_name[i] <= '9')) {

		} else {
			return false;
		}
	}
	return !l_ishan;
}

inline const char* g_GetUseItemFailedInfo(short sErrorID) {
	switch (sErrorID) {
	case enumITEMOPT_SUCCESS:
		return "Item operation succesful"; // "µАѕЯІЩЧчіЙ№¦";
		break;
	case enumITEMOPT_ERROR_NONE:
		return "Equipment does not exist"; // "µАѕЯІ»ґжФЪ";
		break;
	case enumITEMOPT_ERROR_KBFULL:
		return "Inventory is full"; // "µАѕЯАёТСВъ";
		break;
	case enumITEMOPT_ERROR_UNUSE:
		return "Failed to use item"; // "µАѕЯК№УГК§°Ь";
		break;
	case enumITEMOPT_ERROR_UNPICKUP:
		return "µRl??»ДЬE°C?"; // "µАѕЯІ»ДЬК°ИЎ";
		break;
	case enumITEMOPT_ERROR_UNTHROW:
		return "Item cannot be thrown"; // "µАѕЯІ»ДЬ¶ЄЖъ";
		break;
	case enumITEMOPT_ERROR_UNDEL:
		return "Item cannot be destroyed"; // "µАѕЯІ»ДЬПъ»Щ";
		break;
	case enumITEMOPT_ERROR_KBLOCK:
		return "inventory is currently locked"; // "µАѕЯАёґ¦УЪЛш¶ЁЧґМ¬";
		break;
	case enumITEMOPT_ERROR_DISTANCE:
		return "Distance too far"; // "ѕаАлМ«Ф¶";
		break;
	case enumITEMOPT_ERROR_EQUIPLV:
		return "Equipment level mismatch"; // "Ч°±ёµИј¶І»ВъЧг";
		break;
	case enumITEMOPT_ERROR_EQUIPJOB:
		return "Does not meet the class requirement for the equipment"; // "Ч°±ёЦ°ТµІ»ВъЧг";
		break;
	case enumITEMOPT_ERROR_STATE:
		return "Unable to operate items under the current condition"; // "ёГЧґМ¬ПВІ»ДЬІЩЧчµАѕЯ";
		break;
	case enumITEMOPT_ERROR_PROTECT:
		return "Item is being protected"; // "µАѕЯ±»±Ј»¤";
		break;
	case enumITEMOPT_ERROR_AREA:
		return "different region type"; // "І»Н¬µДЗшУтАаРН";
		break;
	case enumITEMOPT_ERROR_BODY:
		return "type of build does not match"; // "МеРНІ»ЖҐЕд";
		break;
	case enumITEMOPT_ERROR_TYPE:
		return "Unable to store this item"; // "ґЛµАѕЯОЮ·Ёґж·Е";
		break;
	case enumITEMOPT_ERROR_INVALID:
		return "Item not in used"; // "ОЮР§µДµАѕЯ";
		break;
	case enumITEMOPT_ERROR_KBRANGE:
		return "out of inventory range"; // "і¬іцµАѕЯАё·¶О§";
		break;
	default:
		return "Unknown item usage failure code"; // "ОґЦЄµДµАѕЯІЩЧчК§°ЬВл";
		break;
	}
}

class CTextFilter {
public:
#define eTableMax 5
	enum eFilterTable { NAME_TABLE = 0,
						DIALOG_TABLE = 1,
						MAX_TABLE = eTableMax };
	/*
	 * Warning : Do not use MAX_TABLE enum value, it just use for the maximum limit definition,
	 *			If you want to expand this enum table value more than the default number eTableMax(5),
	 *			please increase the eTableMax definition
	 */

	CTextFilter();
	~CTextFilter();
	static bool Add(const eFilterTable eTable, const char* szFilterText);
	static bool IsLegalText(const eFilterTable eTable, const std::string strText);
	static bool Filter(const eFilterTable eTable, std::string& strText);
	static bool LoadFile(const char* szFileName, const eFilterTable eTable = NAME_TABLE);
	static BYTE* GetNowSign(const eFilterTable eTable);

private:
	static bool ReplaceText(std::string& strText, const std::string* pstrFilterText);
	static bool bCheckLegalText(const std::string& strText, const std::string* pstrIllegalText);

	static std::vector<std::string> m_FilterTable[eTableMax];
	static BYTE m_NowSign[eTableMax][8];
};

#endif // COMMFUNC_H
#ifndef INIFILE_H
#define INIFILE_H

#include "excp.h"

namespace dbc {
#pragma pack(push)
#pragma pack(4)

struct IniItem {
	std::string name;
	std::string value;
};

class IniSection {
	friend class IniFile;

public:
	std::int32_t ItemCount() const { return m_itemcount; }
	IniItem& operator[](std::int32_t);
	std::string& operator[](const char* name) const;

private:
	IniSection() : m_itemcount(0), m_itemsize(0), m_item(0) {}
	~IniSection();

	IniItem* AddItem(const char* name, const char* value);

	struct {
		std::int32_t m_itemcount;
		std::int32_t m_itemsize;
		IniItem** m_item;
		std::string m_sectname;
	};
};

class IniFile {
public:
	IniFile(const char* filename);
	~IniFile();

	std::int32_t SectCount() const { return m_sectcount; }
	IniSection& operator[](std::int32_t);
	IniSection& operator[](const char* sectname) const;

private:
	void Flush();
	void ReadFile(const char* filename);
	IniSection* AddSection(const char* sectname);

	struct {
		std::int32_t m_sectcount;
		std::int32_t m_sectsize;
		IniSection** m_sect;
	};

	bool m_update;
	bool m_rw; // false:只读;true:读和写。
	char m_filename[512]{};
};

#pragma pack(pop)
} // namespace dbc

#endif
